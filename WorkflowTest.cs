﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using System.Activities.Statements;
using System.Threading;
using Microsoft.PowerShell.Activities;
using System.IO;

namespace DrWorkerService.PowershellSupport
{
    class WorkflowTest
    {
        void TestWorkflow()
        {
            Activity wf = new WriteLine
            {
                Text = "Hello World."
            };
            Activity seq = new Sequence()
            {
                Activities = 
                {
                    new WriteLine() { Text = "Before", DisplayName = "Disp"},
                    new Delay() { Duration = TimeSpan.FromSeconds(10) },
                    new WriteLine() { Text = "After" }
                }
            };
            Dictionary<string, object> inputs = new Dictionary<string, object>();
            inputs.Add("Text", "Hello World.");

            InlineScript ins = new InlineScript();
            ins.Command = "get-services";

            WorkflowInvoker.Invoke(wf, inputs);

            WorkflowInvoker.Invoke(seq);
        }
    }
}

