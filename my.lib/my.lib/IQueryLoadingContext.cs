﻿using System;
using System.Linq.Expressions;

namespace MalkosUa.Data.Query
{
    /// <summary>
    /// Describes contract for classes which implement query execution context.
    /// </summary>
    /// <remarks>
    /// This type of query context allows setting filters, setting loading strategies and query executing.
    /// </remarks>
    /// <typeparam name="TResult">The type of query result.</typeparam>
    public interface IQueryLoadingContext<TResult> : IQueryExecutionContext<TResult>
    {
        #region Methods

        /// <summary>
        /// Sets lazy loading status.
        /// </summary>
        /// <remarks>
        /// Lazy loading is disabled by default.
        /// </remarks>
        /// <param name="lazyLoadingEnabled">The lazy loading status.</param>
        /// <returns>This instance.</returns>
        IQueryLoadingContext<TResult> SetLazyLoading(bool lazyLoadingEnabled);

        /// <summary>
        /// Enables lazy loading.
        /// </summary>
        /// <remarks>
        /// Lazy loading is disabled by default.
        /// </remarks>
        /// <returns>This instance.</returns>
        IQueryLoadingContext<TResult> EnableLazyLoading();

        /// <summary>
        /// Disables lazy loading.
        /// </summary>
        /// <remarks>
        /// Lazy loading is disabled by default.
        /// </remarks>
        /// <returns>This instance.</returns>
        IQueryLoadingContext<TResult> DisableLazyLoading();

        /// <summary>
        /// Enables mode in which loaded entities will not be cached.
        /// </summary>
        /// <returns>This instance.</returns>
        IQueryLoadingContext<TResult> AsNoTracking();

        /// <summary>
        /// Specifies the related objects to include in the query results.
        /// </summary>
        /// <typeparam name="TProperty">The type of the navigation property being included.</typeparam>
        /// <param name="propertyPath">Lambda expression which representing the path to the property to include.</param>
        /// <returns>This instance.</returns>
        IQueryLoadingContext<TResult> Load<TProperty>(Expression<Func<TResult, TProperty>> propertyPath);

        /// <summary>
        /// Specifies the related objects to include in the query results.
        /// </summary>
        /// <remarks>
        /// Property, path to which defined in <paramref name="propertyPath"/>, will be loaded only if <paramref name="condition"/> equals <see langword="true"/>.
        /// </remarks>
        /// <typeparam name="TProperty">The type of the navigation property being included.</typeparam>
        /// <param name="condition">The condition.</param>
        /// <param name="propertyPath">Lambda expression which representing the path to the property to include.</param>
        /// <returns>This instance.</returns>
        IQueryLoadingContext<TResult> Load<TProperty>(bool condition, Expression<Func<TResult, TProperty>> propertyPath);

        #endregion
    }
}