﻿// Decompiled with JetBrains decompiler
// Type: MalkosUa.Contexts.IExecutionContext
// Assembly: MalkosUa.General, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: FE45368D-F14F-4851-A931-898232936261
// Assembly location: D:\git\thecvsi\CoreContent\CoreContentServer\Libraries\MalkosUa\MalkosUa.General.dll

using System;

namespace MalkosUa.Contexts
{
    /// <summary>
    /// Describes contract for classes which implement execution context.
    /// </summary>
    public interface IExecutionContext : IUniqueObject, IEquatable<IExecutionContext>, IDisposable
    {
        /// <summary>
        /// Gets value of the property of the type <typeparamref name="T" /> with the specified <paramref name="name" />.
        /// </summary>
        /// <typeparam name="T">The type of property.</typeparam>
        /// <param name="name">The property name.</param>
        /// <returns>
        /// The value of the property of the type <typeparamref name="T" /> with the specified <paramref name="name" /> or
        /// default value if property with the specified name not found.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">Parameter <paramref name="name" /> is <see langword="null" />.</exception>
        T GetProperty<T>(object name);


        /// <summary>
        /// Gets value of the property of the type <typeparamref name="T" />.
        /// Name of the property will be generated based on the type <typeparamref name="T" />.
        /// </summary>
        /// <typeparam name="T">The type of property.</typeparam>
        /// <returns>
        /// The value of the property of the type <typeparamref name="T" /> or default value if property of the type <typeparamref name="T" /> not found.
        /// </returns>
        T GetProperty<T>();


        /// <summary>
        /// Sets the <paramref name="value" /> of the property of the type <typeparamref name="T" /> with the specified <paramref name="name" />.
        /// </summary>
        /// <typeparam name="T">The type of property.</typeparam>
        /// <param name="name">The property name.</param>
        /// <param name="value">The property value.</param>
        /// <returns>The same <see cref="T:MalkosUa.Contexts.IExecutionContext" /> instance so that multiple calls can be chained.</returns>
        /// <exception cref="T:System.ArgumentNullException">Parameter <paramref name="name" /> is <see langword="null" />.</exception>
        IExecutionContext SetProperty<T>(object name, T value);


        /// <summary>
        /// Sets the <paramref name="value" /> of the property of the type <typeparamref name="T" />.
        /// Name of the property will be generated based on the type <typeparamref name="T" />.
        /// </summary>
        /// <typeparam name="T">The type of property.</typeparam>
        /// <param name="value">The property value.</param>
        /// <returns>The same <see cref="T:MalkosUa.Contexts.IExecutionContext" /> instance so that multiple calls can be chained.</returns>
        IExecutionContext SetProperty<T>(T value);


        /// <summary>
        /// Determines whether the property of the type <typeparamref name="T" /> with the specified <paramref name="name" /> exists.
        /// </summary>
        /// <typeparam name="T">The type of property.</typeparam>
        /// <param name="name">The property name.</param>
        /// <returns>
        /// Returns <see langword="true" /> if property of the type <typeparamref name="T" /> with the specified <paramref name="name" /> exists.
        /// Otherwise - <see langword="false" />.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">Parameter <paramref name="name" /> is <see langword="null" />.</exception>
        bool IsPropertyExists<T>(object name);


        /// <summary>
        /// Determines whether the property of the type <typeparamref name="T" /> exists.
        /// Name of the property will be generated based on the type <typeparamref name="T" />.
        /// </summary>
        /// <typeparam name="T">The type of property.</typeparam>
        /// <returns>
        /// Returns <see langword="true" /> if property of the type <typeparamref name="T" /> exists.
        /// Otherwise - <see langword="false" />.
        /// </returns>
        bool IsPropertyExists<T>();


        /// <summary>
        /// Gets the instance of a service with the specified <paramref name="name" /> and of the specified <paramref name="type" />.
        /// </summary>
        /// <param name="type">The type of the service.</param>
        /// <param name="name">The name of the service.</param>
        /// <returns>
        /// The instance of a service with the specified <paramref name="name" /> and of the specified <paramref name="type" />.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">
        /// Parameters <paramref name="type" /> or/and <paramref name="name" /> is/are <see langword="null" />.
        /// </exception>
        object GetService(Type type, string name);


        /// <summary>
        /// Gets the instance of a service of the specified <paramref name="type" />.
        /// </summary>
        /// <param name="type">The type of the service.</param>
        /// <returns>The instance of a service of the specified <paramref name="type" />.</returns>
        /// <exception cref="T:System.ArgumentNullException">Parameter <paramref name="type" /> is <see langword="null" />.</exception>
        object GetService(Type type);


        /// <summary>
        /// Gets the instance of a service with the specified <paramref name="name" /> and of the specified type.
        /// </summary>
        /// <typeparam name="T">The type of the service.</typeparam>
        /// <param name="name">The name of the service.</param>
        /// <returns>
        /// The instance of a service with the specified <paramref name="name" /> and of the specified type.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">Parameter <paramref name="name" /> is <see langword="null" />.</exception>
        T GetService<T>(string name);


        /// <summary>Gets the instance of a service of the specified type.</summary>
        /// <typeparam name="T">The type of the service.</typeparam>
        /// <returns>The instance of a service of the specified type.</returns>
        T GetService<T>();


        /// <summary>
        /// Determines whether the service with the specified <paramref name="name" /> and of the specified <paramref name="type" /> has been registered.
        /// </summary>
        /// <param name="type">The type of the service.</param>
        /// <param name="name">The name of the service.</param>
        /// <returns>
        /// Returns <see langword="true" /> if the service with the specified <paramref name="name" /> and of the specified <paramref name="type" /> has been registered.
        /// Otherwise - <see langword="false" />.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">
        /// Parameters <paramref name="type" /> or/and <paramref name="name" /> is/are <see langword="null" />.
        /// </exception>
        bool IsServiceRegistered(Type type, string name);


        /// <summary>
        /// Determines whether the service of the specified <paramref name="type" /> has been registered.
        /// </summary>
        /// <param name="type">The type of the service.</param>
        /// <returns>
        /// Returns <see langword="true" /> if the service of the specified <paramref name="type" /> has been registered.
        /// Otherwise - <see langword="false" />.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">Parameter <paramref name="type" /> is <see langword="null" />.</exception>
        bool IsServiceRegistered(Type type);


        /// <summary>
        /// Determines whether the service with the specified <paramref name="name" /> and of the specified type has been registered.
        /// </summary>
        /// <typeparam name="T">The type of the service.</typeparam>
        /// <param name="name">The name of the service.</param>
        /// <returns>
        /// Returns <see langword="true" /> if the service with the specified <paramref name="name" /> and of the specified type has been registered.
        /// Otherwise - <see langword="false" />.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">Parameter <paramref name="name" /> is <see langword="null" />.</exception>
        bool IsServiceRegistered<T>(string name);


        /// <summary>
        /// Determines whether the service of the specified type has been registered.
        /// </summary>
        /// <typeparam name="T">The type of the service.</typeparam>
        /// <returns>
        /// Returns <see langword="true" /> if the service of the specified type has been registered.
        /// Otherwise - <see langword="false" />.
        /// </returns>
        bool IsServiceRegistered<T>();


        /// <summary>Gets elapsed time.</summary>
        TimeSpan Elapsed { get; }
    }
}
