﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MalkosUa.Data.Query
{
    /// <summary>
    /// Describes contract for classes which implement query execution.
    /// </summary>
    /// <remarks>
    /// This type of query context allows only query executing.
    /// </remarks>
    /// <typeparam name="TResult">The type of query result.</typeparam>
    public interface IQueryExecutionContext<TResult>
    {
        #region Collections

        /// <summary>
        /// Returns the query result typed as <see cref="IEnumerable{TResult}"/>.
        /// </summary>
        /// <returns>The result sequence typed as <see cref="IEnumerable{TResult}"/>.</returns>
        IEnumerable<TResult> AsEnumerable();

        /// <summary>
        /// Returns the query result typed as <see cref="IEnumerable{T}"/>.
        /// </summary>
        /// <typeparam name="T">The type of result.</typeparam>
        /// <param name="selector">A projection function to apply to each element.</param>
        /// <returns>The result sequence typed as <see cref="IEnumerable{T}"/>.</returns>
        IEnumerable<T> AsEnumerable<T>(Expression<Func<TResult, T>> selector);

        /// <summary>
        /// Returns the query result typed as <see cref="IList{TResult}"/>.
        /// </summary>
        /// <returns>The result sequence typed as <see cref="IList{TResult}"/>.</returns>
        IList<TResult> ToList();

        /// <summary>
        /// Returns the query result typed as <see cref="IList{T}"/>.
        /// </summary>
        /// <typeparam name="T">The type of result.</typeparam>
        /// <param name="selector">A projection function to apply to each element.</param>
        /// <returns>The result sequence typed as <see cref="IList{T}"/>.</returns>
        IList<T> ToList<T>(Expression<Func<TResult, T>> selector);

        /// <summary>
        /// Returns the query result typed as array.
        /// </summary>
        /// <returns>The result sequence typed as array.</returns>
        TResult[] ToArray();

        /// <summary>
        /// Returns the query result typed as array.
        /// </summary>
        /// <typeparam name="T">The type of result.</typeparam>
        /// <param name="selector">A projection function to apply to each element.</param>
        /// <returns>The result sequence typed as array.</returns>
        T[] ToArray<T>(Expression<Func<TResult, T>> selector);

        #endregion

        #region Scalars

        /// <summary>
        /// Returns the only element of the query result and throws an exception if there is not exactly one element in the query result.
        /// </summary>
        /// <returns>The single element of the query result.</returns>
        /// <exception cref="InvalidOperationException">The query result contains more than one element.</exception>
        /// <exception cref="InvalidOperationException">The query result is empty.</exception>
        TResult Single();

        /// <summary>
        /// Returns the only element of the query result that satisfies a specified condition and throws an exception if there is not exactly one element in the query result.
        /// </summary>
        /// <typeparam name="T">The type of result.</typeparam>
        /// <param name="selector">A projection function to apply to each element.</param>
        /// <returns>The single element of the query result.</returns>
        /// <exception cref="InvalidOperationException">The query result contains more than one element.</exception>
        /// <exception cref="InvalidOperationException">The query result is empty.</exception>
        T Single<T>(Expression<Func<TResult, T>> selector);

        /// <summary>
        /// Returns the only element of the query result or a default value if the query result is empty.
        /// This method throws an exception if there is more than one element in the query result.
        /// </summary>
        /// <returns>The single element of the query result or default value if the query result contains no elements.</returns>
        /// <exception cref="InvalidOperationException">The query result contains more than one element.</exception>
        TResult SingleOrDefault();

        /// <summary>
        /// Returns the only element of the query result or a default value if the query result is empty.
        /// This method throws an exception if there is more than one element in the query result.
        /// </summary>
        /// <typeparam name="T">The type of result.</typeparam>
        /// <param name="selector">A projection function to apply to each element.</param>
        /// <returns>The single element of the query result or default value if the query result contains no elements.</returns>
        /// <exception cref="InvalidOperationException">The query result contains more than one element.</exception>
        T SingleOrDefault<T>(Expression<Func<TResult, T>> selector);

        /// <summary>
        /// Returns the first element of the query result.
        /// </summary>
        /// <returns>The first element of the query result.</returns>
        TResult First();

        /// <summary>
        /// Returns the first element of the query result.
        /// </summary>
        /// <typeparam name="T">The type of result.</typeparam>
        /// <param name="selector">A projection function to apply to each element.</param>
        /// <returns>The first element of the query result.</returns>
        T First<T>(Expression<Func<TResult, T>> selector);

        /// <summary>
        /// Returns the first element of the query result or a default value if the query result contains no elements.
        /// </summary>
        /// <returns>Default value if the query result is empty. Otherwise, the first element in the query result.</returns>
        TResult FirstOrDefault();

        /// <summary>
        /// Returns the first element of the query result or a default value if the query result contains no elements.
        /// </summary>
        /// <typeparam name="T">The type of result.</typeparam>
        /// <param name="selector">A projection function to apply to each element.</param>
        /// <returns>Default value if the query result is empty. Otherwise, the first element in the query result.</returns>
        T FirstOrDefault<T>(Expression<Func<TResult, T>> selector);

        #endregion

        /// <summary>
        /// Returns the number of elements in the query.
        /// </summary>
        /// <returns>The number of elements in the query.</returns>
        int Count();

        /// <summary>
        /// Returns an <see cref="System.Int64"/> that represents the total number of elements in the query.
        /// </summary>
        /// <returns>The number of elements in the query.</returns>
        long LongCount();

        /// <summary>
        /// Turns projection on.
        /// </summary>
        /// <returns>This instance.</returns>
        IQueryExecutionContext<TResult> Project();

        /// <summary>
        /// Disables projection.
        /// </summary>
        /// <returns>This instance.</returns>
        IQueryExecutionContext<TResult> DisableProject();

        /// <summary>
        /// Clears result of query executing.
        /// </summary>
        /// <returns>This instance.</returns>
        IQueryExecutionContext<TResult> ClearResult();
    }
}