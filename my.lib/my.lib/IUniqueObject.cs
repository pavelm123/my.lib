﻿namespace MalkosUa.Contexts
{

    /// <summary>
    /// Describes contract for object instances which have unique Id.
    /// </summary>
    public interface IUniqueObject
    {
        /// <summary>Gets a unique Id of this instance.</summary>
        string UniqueId { get; }
    }

}
