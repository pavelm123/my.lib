using System;
using MalkosUa.Data.Query;
using Xunit;
using Moq;

namespace my.lib.test
{
    public class TestClass
    {
        public string S;
        public int I;
        public double D;
    }

    public interface IFoo
    {
        bool DoSomething(string value);
        string DoSomethingStringy(string value);
        bool TryParse(string value, out string outputValue);
        bool Submit(ref TestClass bar);
        int GetCount();
        int GetCountThing();
    }

    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var mockedInterface = new Mock<IFoo>(MockBehavior.Strict);// Default is "Loose"
            mockedInterface.Setup(x => x.GetCount()).Returns(5);
            mockedInterface.Setup(x=>x.DoSomethingStringy(It.IsAny<>()))

            mockedInterface.Setup(x=>x.GetSer .Setup(x => x.Count()).Returns(5);
        }
    }
}
