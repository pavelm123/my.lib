﻿using System;
using BugReproducingConsole.Utils;

namespace multisetcomparer
{
    class Program
    {
        static void Main(string[] args)
        {
            Guid?[] ar1 = {
                Guid.Parse("00000000-0000-0000-0000-000000000001"),
                null,
                null,
                Guid.Parse("00000000-0000-0000-0000-000000000002"),
                Guid.Parse("00000000-0000-0000-0000-000000000003")
            };
            Guid?[] ar1nonull = {
                Guid.Parse("00000000-0000-0000-0000-000000000001"),
                null,
                Guid.Parse("00000000-0000-0000-0000-000000000002"),
                Guid.Parse("00000000-0000-0000-0000-000000000003")
            };
            Guid?[] ar2 = {
                Guid.Parse("00000000-0000-0000-0000-000000000001"),
                null,
                null,
                Guid.Parse("00000000-0000-0000-0000-000000000002"),
                Guid.Parse("00000000-0000-0000-0000-000000000003")
            };
            Guid?[] ar3 = {
                Guid.Parse("00000000-0000-0000-0000-000000000001"),
                Guid.Parse("00000000-0000-0000-0000-000000000001"),
                null,
                null,
                Guid.Parse("00000000-0000-0000-0000-000000000002"),
                Guid.Parse("00000000-0000-0000-0000-000000000003")
            };
            Guid?[] ar4 = {
                Guid.Parse("00000000-0000-0000-0000-000000000001"),
                Guid.Parse("00000000-0000-0000-0000-000000000002"),
                Guid.Parse("00000000-0000-0000-0000-000000000003")
            };
            Guid?[] ar5 = {
                Guid.Parse("00000000-0000-0000-0000-000000000001"),
                Guid.Parse("00000000-0000-0000-0000-000000000002"),
                Guid.Parse("00000000-0000-0000-0000-000000000003"),
                Guid.Parse("00000000-0000-0000-0000-000000000004")
            };
            Guid?[] ar6equaldifferentorder = {
                Guid.Parse("00000000-0000-0000-0000-000000000003"),
                Guid.Parse("00000000-0000-0000-0000-000000000002"),
                Guid.Parse("00000000-0000-0000-0000-000000000001"),
                null,
                null
            };
            Guid?[] ar7 = {
                Guid.Parse("00000000-0000-0000-0000-000000000001"),
                Guid.Parse("00000000-0000-0000-0000-000000000001"),
                null,
                null,
                Guid.Parse("00000000-0000-0000-0000-000000000002"),
                Guid.Parse("00000000-0000-0000-0000-000000000003"),
                Guid.Parse("00000000-0000-0000-0000-000000000004"),
                Guid.Parse("00000000-0000-0000-0000-000000000004")
            };

            var comp = new MultiSetComparer<Guid?>();

            comp.Equals(ar1, ar1, PrintIt, nameof(ar1), nameof(ar1));
            Console.WriteLine("next text===========================");
            comp.Equals(null, ar1, PrintIt, "null", nameof(ar1));
            Console.WriteLine("next text===========================");
            comp.Equals(ar1, null, PrintIt, nameof(ar1), "null");
            Console.WriteLine("next text===========================");

            comp.Equals(ar1, ar2, PrintIt, nameof(ar1), nameof(ar2));
            Console.WriteLine("next text===========================");
            comp.Equals(ar1, ar1nonull, PrintIt, nameof(ar1), nameof(ar1nonull));
            Console.WriteLine("next text===========================");

            
            comp.Equals(ar1, ar3, PrintIt, nameof(ar1), nameof(ar3));
            Console.WriteLine("next text===========================");
            comp.Equals(ar1, ar4, PrintIt, nameof(ar1), nameof(ar4));
            Console.WriteLine("next text===========================");
            comp.Equals(ar1, ar5, PrintIt, nameof(ar1), nameof(ar5));

            Console.WriteLine("next text===========================");
            comp.Equals(ar1, ar6equaldifferentorder, PrintIt, nameof(ar1), nameof(ar6equaldifferentorder));

            Console.WriteLine("next text===========================");
            comp.Equals(ar1, ar7, PrintIt, nameof(ar1), nameof(ar7));

            Console.ReadKey();
            
        }
        private static void PrintIt<T>(string message ,T value)
        {
            Console.WriteLine("{0}: {1}", message, value);
        }
    }
}