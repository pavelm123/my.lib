﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
https://stackoverflow.com/questions/50098/comparing-two-collections-for-equality-irrespective-of-the-order-of-items-in-the


Sample usage:

var set = new HashSet<IEnumerable<int>>(new[] {new[]{1,2,3}}, new MultiSetComparer<int>());
Console.WriteLine(set.Contains(new [] {3,2,1})); //true
Console.WriteLine(set.Contains(new [] {1, 2, 3, 3})); //false
Or if you just want to compare two collections directly:

var comp = new MultiSetComparer<string>();
Console.WriteLine(comp.Equals(new[] {"a","b","c"}, new[] {"a","c","b"})); //true
Console.WriteLine(comp.Equals(new[] {"a","b","c"}, new[] {"a","b"})); //false
Finally, you can use your an equality comparer of your choice:

var strcomp = new MultiSetComparer<string>(StringComparer.OrdinalIgnoreCase);
Console.WriteLine(strcomp.Equals(new[] {"a", "b"}, new []{"B", "A"})); //true

 * 
 * * 
 */


namespace BugReproducingConsole.Utils
{
    /// <summary>
    /// Compares and prints all unmatched elements in two sets (UNORDERED lists!, meaning 1, 2, 3 and 3, 2, 1 are considered SAME)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MultiSetComparer<T> : IEqualityComparer<IEnumerable<T>>
    {
        private readonly IEqualityComparer<T> _comparer;

        /// <summary>
        /// Use new MultiSetComparer<string>(StringComparer.OrdinalIgnoreCase); to compare set of strings
        /// </summary>
        /// <param name="comparer"></param>
        public MultiSetComparer(IEqualityComparer<T> comparer = null)
        {
            _comparer = comparer ?? EqualityComparer<T>.Default;
        }

        public bool Equals(IEnumerable<T> first, IEnumerable<T> second)
        {
            return Equals(first, second, null);
        }

        public bool Equals(IEnumerable<T> first, IEnumerable<T> second, Action<string, T> action = null, string firstSequenceName = "first", string secondSequenceName = "second")
        {
            if (ReferenceEquals(first, second)) return ReturnTrue(action);

            if (first == null)
            {
                if (second == null) ReturnTrue(action);
                ExistOnlyInSequence($"Exist only in {secondSequenceName}", second, action);
                return false;
            }
            if (second == null)
            {
                ExistOnlyInSequence($"Exist only in {firstSequenceName}", first, action);
                return false;
            }

            int firstNullCount;
            int secondNullCount;
            bool returnValue = true;

            var firstElementCounts = GetElementCounts(first, out firstNullCount);
            var secondElementCounts = GetElementCounts(second, out secondNullCount);

            if (firstNullCount != 0 || secondNullCount != 0)
            {
                // i need this now... can be removed later, since it's more like and infomrational item...
                if (firstNullCount == secondNullCount)
                    action?.Invoke($"Sequence contains same number ({firstNullCount}) of NULL values", default(T));

                if (firstNullCount != secondNullCount)
                {
                    action?.Invoke($"Different number of NULL values in sequences! First = {firstNullCount}, second={secondNullCount}", default(T));
                    returnValue = false;
                }
            }

            if (firstElementCounts.Count != secondElementCounts.Count)
            {
                action?.Invoke($"Sequences have different numbers of unique elements: First = {firstElementCounts.Count}, second={secondElementCounts.Count}", default(T));
                returnValue = false;
            }

            foreach (var kvp in firstElementCounts)
            {
                var firstElementCount = kvp.Value;
                int secondElementCount = -1;
                bool tryGetResult = secondElementCounts.TryGetValue(kvp.Key, out secondElementCount);

                if (!tryGetResult)
                {
                    action?.Invoke($"Only in first sequence", kvp.Key);
                    returnValue = false;
                }
                else
                {
                    if (firstElementCount != secondElementCount)
                    {
                        action?.Invoke($"Element counts are not the same. First = {firstElementCount}, second={secondElementCount}", kvp.Key);
                        returnValue = false;
                    }
                    secondElementCounts.Remove(kvp.Key);
                }
            }
            if (secondElementCounts.Count != 0)
            {
                foreach (var kvp in secondElementCounts)
                {
                    action?.Invoke($"Only second sequence has {kvp.Value} elements", kvp.Key);
                }
                returnValue = false;
            }

            if (returnValue) return ReturnTrue(action);
            return false;
        }

        private bool ReturnTrue(Action<string, T> action = null)
        {
            action?.Invoke("Both sequences are equal", default(T));
            return true;
        }

        private void ExistOnlyInSequence(string title, IEnumerable<T> sequence, Action<string, T> action = null)
        {
            foreach (var el in  sequence)
            {
                action?.Invoke(title, el);
            }
        }

        private Dictionary<T, int> GetElementCounts(IEnumerable<T> enumerable, out int nullCount)
        {
            var dictionary = new Dictionary<T, int>(_comparer);
            nullCount = 0;

            foreach (T element in enumerable)
            {
                if (element == null)
                {
                    nullCount++;
                }
                else
                {
                    int num;
                    dictionary.TryGetValue(element, out num);
                    num++;
                    dictionary[element] = num;
                }
            }

            return dictionary;
        }

        public int GetHashCode(IEnumerable<T> enumerable)
        {
            if (enumerable == null) throw new ArgumentNullException(nameof(enumerable));

            int hash = 17;

            foreach (T val in enumerable.OrderBy(x => x))
                hash = hash * 23 + (val?.GetHashCode() ?? 42);

            return hash;
        }
    }
}
